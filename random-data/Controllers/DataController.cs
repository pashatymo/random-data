﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace random_data.Controllers
{
    public class DataController : ApiController
    {
        [HttpGet]
        [Route("Add")]
        public HttpResponseMessage FillDb()
        {
            try
            {
                using (RandomDataEntities context = new RandomDataEntities())
                {
                    context.Stores.Add(new Store
                    {
                        date = DateTime.UtcNow,
                        first = Guid.NewGuid().ToString(),
                        second = Guid.NewGuid().ToString(),
                        third = Guid.NewGuid().ToString()
                    });
                    context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("GetData")]
        public List<Store> Get()
        {
            var result = new List<Store>();
            using (RandomDataEntities context = new RandomDataEntities())
            {
                result = context.Stores.ToList();
            };
            return result;
        }
    }
}
